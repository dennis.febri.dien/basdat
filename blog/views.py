from django.shortcuts import render
from django.http import HttpResponse

def createNewsIndex(request):
    return render(request, 'createNews.html')

def createPollNewsIndex(request):
    return render(request, 'createPollNews.html')

def createPollNormalIndex(request):
    return render(request, 'createPollNormal.html')


# Create your views here.
