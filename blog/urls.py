
from django.conf.urls import url
from .views import createNewsIndex
from .views import createPollNewsIndex
from .views import createPollNormalIndex
from . import views

urlpatterns = [
	url(r'^create_news/', createNewsIndex, name='createNews'),
	url(r'^create_poll/news', createPollNewsIndex, name='createPollNews'),
	url(r'^create_poll/normal', createPollNormalIndex, name='createPollNormal'),
]